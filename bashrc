#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export EDITOR=vim

alias ls='ls --color=auto'
alias l='ls -la'
alias v='vim'
alias c='clear'
alias r='rm -i'
alias tree='tree -C'
alias i3lock='i3lock -i /home/antonin/.confs/background.png'
alias gccc='gcc -Wall -Wextra -Werror -std=c99 -pedantic'
alias clone='history -a && i3-sensible-terminal &'

PS1='\u@\h:\w\[\033[00m\]\$ '
